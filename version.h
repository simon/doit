/*
 * In source distribution archives, this header file is overwritten
 * with one that defines VERSION to a string literal matching the
 * version of the source distribution archive.
 *
 * Building from git, you get this style of version.h which leaves
 * VERSION not defined at all.
 */

#undef VERSION
