#include <windows.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#include "msgbuf.h"

void msgbuf_ensure(struct msgbuf *mb, int size_needed)
{
    if (mb->size < size_needed) {
        mb->size = size_needed * 5 / 4 + 256;
        mb->buf = realloc(mb->buf, mb->size);
    }
}

char *msgbuf_endptr(struct msgbuf *mb, int len_needed)
{
    msgbuf_ensure(mb, mb->len + len_needed);
    return mb->buf ? mb->buf + mb->len : NULL;
}

void msgbuf_append(struct msgbuf *mb, const char *str)
{
    int len = strlen(str);
    char *ptr = msgbuf_endptr(mb, len);
    if (ptr) {
        memcpy(ptr, str, len);
        mb->len += len;
    }
}

const char *msgbuf_sz(struct msgbuf *mb)
{
    char *endptr = msgbuf_endptr(mb, 1);
    if (!endptr)
        return NULL;
    *endptr = '\0';
    return mb->buf;
}
