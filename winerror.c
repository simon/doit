#include <windows.h>
#include <stdio.h>
#include <string.h>

#include "msgbuf.h"

void msgbuf_append_win_error(struct msgbuf *mb, unsigned error)
{
    char *ptr;

    if ((ptr = msgbuf_endptr(mb, 64)) != NULL) {
        mb->len += sprintf(ptr, "Error %u: ", error);
    }

    if ((ptr = msgbuf_endptr(mb, 0x10000)) != NULL) {
        if (!FormatMessage((FORMAT_MESSAGE_FROM_SYSTEM |
                            FORMAT_MESSAGE_IGNORE_INSERTS), NULL, error,
                           MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                           ptr, 0xFFFF, NULL)) {
            mb->len += sprintf(ptr,
                               "(unable to format: FormatMessage returned %u)",
                               (unsigned int)GetLastError());
        } else {
            mb->len += strlen(ptr);
            while (ptr[0] && (mb->buf[mb->len - 1] == '\n' ||
                              mb->buf[mb->len - 1] == '\r'))
                mb->len--;
        }
    }
}

void msgbuf_getlasterror(struct msgbuf *mb)
{
    msgbuf_append_win_error(mb, GetLastError());
}

void msgbuf_wsagetlasterror(struct msgbuf *mb)
{
    msgbuf_append_win_error(mb, WSAGetLastError());
}
