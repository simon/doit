/*
 * msgbuf.h: define a small struct for accumulating error messages.
 */

#ifndef DOIT_MSGBUF_H
#define DOIT_MSGBUF_H

struct msgbuf {
    char *buf;
    int len, size;
};

#define MSGBUF_INIT { NULL, 0, 0 }

void msgbuf_ensure(struct msgbuf *mb, int size_needed);
char *msgbuf_endptr(struct msgbuf *mb, int len_needed);
void msgbuf_append(struct msgbuf *mb, const char *str);
const char *msgbuf_sz(struct msgbuf *mb);

void msgbuf_append_win_error(struct msgbuf *mb, unsigned error);
void msgbuf_getlasterror(struct msgbuf *mb);
void msgbuf_wsagetlasterror(struct msgbuf *mb);

#endif /* DOIT_MSGBUF_H */
